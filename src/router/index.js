import Vue from 'vue'
import Router from 'vue-router'
import CodeMirror from '@/components/CodeMirror'
// import SillyText from '@/components/SillyText'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: CodeMirror,
      props: true
    }
  ]
})
